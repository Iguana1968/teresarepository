import java.security.SecureRandom;
import java.time.LocalTime;

public class AlimentarDados {

	private static SecureRandom random = new SecureRandom();

	private static String[] categories = { "Meat", "Fish", "Vegetarian", "Salads" };
	private static String[] first = { "Andr�", "Ant�nio", "Maria", "Ana", "Joana", "Pedro", "Rui", "Miguel" };
	private static String[] last = { "Andrade", "Neves", "Serrano", "Costa", "Vieira", "Rebelo", "Soares", "Torr�o" };

	public static Receita buildRecipe(Utilizador user) {
		int r = random.nextInt(30) + 1;
		String title = "recipe " + r;
		String instruction = "instruction " + r;
		String category = categories[(int) (Math.random() * categories.length)];

		return new Receita(title, instruction, category, LocalTime.of(0, 10), user);
	}

	public static Utilizador buildUser(boolean admin) {
		int r = random.nextInt(first.length);
		String name = first[r] + " " + last[r];
		Conta conta = new Conta(name.split(" ")[0].toLowerCase().charAt(0) + name.split(" ")[1].toLowerCase(), "23456");

		return new Utilizador(name, admin, conta);
	}

}
