
public class Conta {

	private String userName;
	private String password;

	public Conta(String userName, String password) {
		this.userName = userName;
		this.password = password;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean validateAccount(String userName, String password) {
		return this.userName.equals(userName) && this.password.equals(password);
	}

}
