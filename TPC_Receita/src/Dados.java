import java.util.ArrayList;

public class Dados {

	private static Dados INSTANCE = new Dados(20);

	private ArrayList<Receita> recipes = new ArrayList<>();

	private ArrayList<Utilizador> users;

	private Dados() {
		recipes = new ArrayList<>();
		users = new ArrayList<>();
	}

	private Dados(int number) {
		this();
		seedData(number);
	}

	public static Dados getInstance(int number) {
		if (INSTANCE == null)
			INSTANCE = new Dados(number);

		return INSTANCE;
	}

	private void seedData(int number) {
		Utilizador user = new Utilizador("Filipe Coelho", true, new Conta("fcoelho", "12345"));

		users.add(user);
		user = new Utilizador("Miguel Coelho", false, new Conta("mcoelho", "12345"));
		users.add(user);

		for (int i = 0; i < number; i++) {
			user = AlimentarDados.buildUser(false);
			users.add(user);
			recipes.add(AlimentarDados.buildRecipe(user));
			recipes.add(AlimentarDados.buildRecipe(user));
			recipes.add(AlimentarDados.buildRecipe(user));
		}
	}

	public ArrayList<Receita> getReceitas() {
		return new ArrayList<>(recipes);
	}

	public ArrayList<Utilizador> getUtilizadores() {
		return new ArrayList<>(users);
	}

	public void addRecipe(Receita recipe) {
		recipes.add(recipe);
	}

	public boolean addUser(Utilizador user) {
		for (Utilizador u : users) {
			if (u.getAccount().getUserName().equals(user.getAccount().getUserName())) {
				return false;
			}
		}
		users.add(user);
		return true;
	}
}
