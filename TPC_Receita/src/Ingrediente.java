
public class Ingrediente {

	private int quantity;
	private String unit;
	private String product;

	public Ingrediente(int quantity, String unit, String product) {
		this.quantity = quantity;
		this.unit = unit;
		this.product = product;
	}

	private int getQuantity() {
		return quantity;
	}

	private String getUnit() {
		return unit;
	}

	private String getProduct() {
		return product;
	}

	private void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	private void setUnit(String unit) {
		this.unit = unit;
	}

	private void setProduct(String product) {
		this.product = product;
	}

	
}

