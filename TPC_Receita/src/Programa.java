import java.time.LocalTime;
import java.util.Scanner;

public class Programa {
	
		private static Scanner scanner = new Scanner(System.in);
		private static Dados data = Dados.getInstance(20);

		private static Utilizador loggedUser;

		public static void main(String[] args) {

			
			boolean exit = false;

			do {

				System.out.println("1. View all recipes");
				System.out.println("2. Login");
				System.out.println("0. Quit");
				System.out.print("Enter choice: ");
				String choice = scanner.nextLine();

				switch (choice) {
				case "1":
					viewAllRecipes();
					break;
				case "2":
					login();
					break;
				case "0":
					exit = true;
					break;
				}

			} while (!exit);

		}

		public static void viewAllRecipes() {
			for (Receita receita : data.getReceitas()) {
				String str = receita.printDetails(loggedUser);
				if (str.length() > 0)
					System.out.println(str);
			}
		}

		public static void login() {
			boolean exit = false;
			do {
				System.out.println("Please, enter credentials");
				System.out.print("Username: ");
				String userName = scanner.nextLine();
				System.out.print("Password: ");
				String password = scanner.nextLine();

				for (Utilizador user : data.getUtilizadores()) {
					if (user.getAccount().validateAccount(userName, password))
						loggedUser = user;
				}

				if (loggedUser == null) {
					System.out.println("wrong credentials.");
					System.out.print("Try again (y/n)");
					String choice = scanner.nextLine();

					if (choice.equals("y"))
						exit = false;
					else
						exit = true;
				} else {
					System.out.println("Welcome " + loggedUser.getName());
					loggedInMenu();
					exit = true;
				}

			} while (!exit);
		}

		public static void loggedInMenu() {
			boolean logout = false;
			do {
				System.out.println("1. Edit personal data");
				System.out.println("2. View own recipes");
				System.out.println("2. View recepies");
				System.out.println("3. Add recipe");

				System.out.println("0. Logout");
				System.out.print("Enter choice: ");
				String choice = scanner.nextLine();

				switch (choice) {
				case "1":
					editPersonalData();
					break;
				case "2":
					viewAllRecipes();
					break;
				case "3":
					addRecipe();
					break;
				case "0":
					loggedUser = null;
					logout = true;
					break;
				default:
					System.out.println("Please, enter a valid choice");
					break;
				}
			} while (!logout);
		}

		private static void addRecipe() {
			System.out.println("Enter the following recipe details");
			System.out.print("Enter title");
			String title = scanner.nextLine();
			System.out.print("Enter instructions");
			String instructions = scanner.nextLine();
			System.out.print("Enter category");
			String category = scanner.nextLine();
			System.out.print("Enter duration (1:02");
			LocalTime duration = LocalTime.parse(scanner.nextLine());

			Receita recipe = new Receita(title, instructions, category, duration, loggedUser);
			data.addRecipe(recipe);
		}

		private static void editPersonalData() {
			System.out.println("Info: " + loggedUser.printDetails());
			System.out.println("1. change name");
			System.out.println("2. change password");
			System.out.println("0. cancel");
			String choice = scanner.nextLine();

			switch (choice) {
			case "1":
				System.out.print("Enter new name");
				String name = scanner.nextLine();
				loggedUser.setName(name);
				break;
			case "2":
				boolean correct = false;
				do {

					System.out.print("Enter new password");
					String password = scanner.nextLine();
					System.out.print("Reenter new password");
					String confirm = scanner.nextLine();

					if (password.equals(confirm)) {
						loggedUser.getAccount().setPassword(password);
						correct = true;
					} else
						System.out.println("Password don't match. Try again");

				} while (!correct);
				break;
			}
		}


}
