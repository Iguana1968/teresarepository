import java.util.ArrayList;

public class Utilizador {

	private String name;
	private boolean administrator;
	private boolean blocked;
	private Conta account;

	public ArrayList<Receita> receitas;

	public Utilizador(String name, boolean administrator, Conta account) {
		this.name = name;
		this.administrator = administrator;
		this.account = account;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Conta getAccount() {
		return account;
	}

	public void setAccount(Conta account) {
		this.account = account;
	}

	public boolean isAdministrator() {
		return administrator;
	}

	public void setAdministrator(boolean administrator) {
		this.administrator = administrator;
	}

	public void toggleAccount(Utilizador user) {
		if (user.isAdministrator())
			this.blocked = !this.blocked;
	}

	public String printDetails() {
		return String.format("%S", name);
	}
}
