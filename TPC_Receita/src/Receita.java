import java.time.LocalTime;
import java.util.ArrayList;

public class Receita {

	private String title;
	private String instructions;
	private String category;
	private LocalTime duration;
	private Dificuldade dificuldade;
	private boolean valid;
	private ArrayList<Ingrediente> ingredients;
	private Utilizador user;
	private int likes;

	public Receita(String title, String instructions, Utilizador user) {
		this.title = title;
		this.instructions = instructions;
		this.user = user;
		if (user.isAdministrator())
			this.valid = true;
		else
			this.valid = false;
	}

	public Receita(String title, String instructions, String category, LocalTime duration, Utilizador user) {
		this(title, instructions, user);
		this.category = category;
		this.duration = duration;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getInstructions() {
		return instructions;
	}

	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public LocalTime getDuration() {
		return duration;
	}

	public void setDuration(LocalTime duration) {
		this.duration = duration;
	}

	public ArrayList<Ingrediente> getIngredients() {
		return ingredients;
	}

	public void setIngredients(ArrayList<Ingrediente> ingredients) {
		this.ingredients = ingredients;
	}

	public Utilizador getUser() {
		return user;
	}

	public void setUser(Utilizador user) {
		this.user = user;
	}

	public int getLikes() {
		return likes;
	}

	public void like() {
		likes++;
	}

	public void dislike() {
		likes--;
	}

	// POJO - Plain Old Java Object
	public boolean isValid() {
		return this.valid;
	}

	public void validateRecepe(Utilizador user) {
		if (user.isAdministrator()) {
			this.valid = true;
		}
	}

	public String printDetails(Utilizador user) {
		String str = "";
		if (user != null) {
			if (user.isAdministrator()) {
				str = String.format("%s", title);
			}
		} else {
			if (this.isValid())
				str = String.format("%s", title);
			else
				str = "";
		}

		return str;
	}

	public String printDetails() {
		if (this.isValid())
			return String.format("%s", title);
		else
			return null;
	}
}

enum Dificuldade {
	FACIL,
	MEDIO,
	DIFICIL
}
