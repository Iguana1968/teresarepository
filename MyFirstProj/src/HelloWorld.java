import java.util.Scanner;

public class HelloWorld {
	static String teste;

	public static void main(String[] args) {
//		int a = 12;
//		Integer n = a; // Autoboxing
//		Integer z = 1;
//		int sum = (n + z);
//		a = n;
//		// float d = 12.0;
//		n.byteValue();
//		String age = "35";
//		int convAge = Integer.parseInt(age);
//		System.out.println(age + 1);
//		System.out.println(convAge);
//		System.out.println(convAge + 1);
//		Scanner scanner = new Scanner(System.in);
//		System.out.print("Enter a number: ");
//		int number = scanner.nextInt();
//		System.out.println(number + 1);
//		//Operators:
//		String str1 = 1 + 1 + ""; // 2
//		String str2 = "" + 1 + 1; // 11
//		System.out.println(1 + 1);
//		System.out.println("  " + 1 + 1);
//		System.out.println(10 / 2); // 5
//		System.out.println(10 % 3); // 1
//		int x = 12;
//		x = x + 1; // 13
//		x++; // 14
//		System.out.println(x); // 15

//		++x; // 15
//		System.out.println(x++); // 15 x = 16
//		System.out.println(x); // 16
//		System.out.println(++x); // 17
//		x--;
//		x = x - 1;
//		int y = 10;
//		System.out.println(y); // 10
//		int a = y++;
//		System.out.println(a + " " + y); // a: 10 y: 11
//		int b = ++y;
//		System.out.println(b + " " + y); // b: 12 y: 12
//		int c = y--;
//		System.out.println(c + " " + y); // c: 12 y: 11
//		int d = --y;
//		System.out.println(d + " " + y); // d: 10 y: 10
//		var division = (double) 10 / (double) 3; // 10.0 / 3.0
//		System.out.println(division); // 3.3333
//		division = 10.0 / 3.0;
//		System.out.println(division); // 3.333
//		division = 10.0 / 3;
//		System.out.println(division);
//		System.out.println(10 / 0);
//		String str1 = "a";
//		String str2 = new String("a");
//		System.out.println(str1);
//		System.out.println(str2);
//		System.out.println(str1 == str2);
//		System.out.println(str1.equals(str2));
//		int x = 10;
//		x = x + 12;
//		x -= 12;
//		System.out.println(x);

//		for(int i= 0; i<3;i++) {
//			System.out.println(i);
//		}
//	String str ="123";
//	int convertStr = Integer.parseInt(str);

		System.out.println(Static.color);
		Static s1 = new Static();
		System.out.println(s1.color);
		Static.color = "RED";
		System.out.println(s1.color);
		Static s2 = new Static();
		System.out.println(s2.color);
		
		

	}
};
