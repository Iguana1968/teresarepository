
public class WaterBottle {
	private String brand;
	private boolean empty;
	public static float code;

	public static void main(String[] args) {
		WaterBottle wb = new WaterBottle();
		System.out.println("Empty = " + wb.empty);
		System.out.println("Brand = " + wb.brand);
		System.out.println("Code = " + code);

		// var spring = null;
		var fall = "leaves";
//		var evening = 2; evening = null;
		var night = new Object();
		var day = 1 / 0;
		// var winter = 12, cold =1;
		// var fall = 2, autumn = 2;
		var morning = "";
		morning = null;

	}
}