
public class Neighborhood {

	public static void main(String[] args) {

		House house1 = new House("RED");
//		house1.printColor();
		House house2 = new House("GREEN");
//		house2.printColor();
		House house3 = new House("BLUE");
//		house3.printColor();

		// House temp = null;
		House temp = house1;
		temp.printColor();
		house1.setColor("Green");
		house1.printColor();
		house1 = null;
		house2.printColor();
		house3.printColor();
		temp.printColor();
		house2 = house3;
		house3 = house1;
		temp = null;
		house2.printColor();
		house3.printColor();
		house1.printColor();
		//temp.printColor();

	}

}
