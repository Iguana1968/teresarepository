
public class House {

	private String color;

	public House(String color) {
		setColor(color);
	}

	public void printColor() {
		System.out.println(color + " " + this.hashCode());
	}

	public void setColor(String color) {

		final String BEGIN_COLOR = "\u001b[";
		final String END_COLOR = "\u001b[0m";
		String code = null;

		switch (color.toLowerCase()) {
		case "red":
			code = "31";
			break;
		case "green":
			code = "32";
			break;
		case "blue":
			code = "34";
			break;
		}

		this.color = BEGIN_COLOR + code + "m" + color.toUpperCase() + END_COLOR;

	}

}
