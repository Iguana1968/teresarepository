import java.util.ArrayList;

public class Sequence {
	int firstNumber = 0;
	int secondNumber = 1;
	final int SEQUENCE_LIMIT = 100;
	int firstNumberArray[];
	int secondNumberArray[];

	public void displaySequence() {
		int nextNumber = 0;
		System.out.println(String.format("%s %s", firstNumber, secondNumber));

		nextNumber = firstNumber + secondNumber;
		while (nextNumber <= SEQUENCE_LIMIT) {
			firstNumberArray[nextNumber] = firstNumber;
			secondNumberArray[nextNumber] = firstNumber;

			firstNumber = secondNumber;
			secondNumber = nextNumber;
			nextNumber = firstNumber + secondNumber;
		}
		System.out.println();
	}
}
