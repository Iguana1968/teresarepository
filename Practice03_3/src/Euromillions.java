
public class Euromillions {

	public static void main(String[] args) {
		int randomNum = 0;
		int guess;
		String strOut;

		if (args.length == 0 || args[0].equals("Help")) {
			System.out.println("Enter a number when calling this application. Or enter help to bring this help text");

		} else {
			randomNum = ((int) (Math.random() * 5) + 1);
			guess = Integer.parseInt(args[0]);
			if (randomNum == guess) {
				strOut = String.format("Congratulations the random number is %d and your guess is %d", randomNum,
						guess);
				System.out.println(strOut);
			} else {
				strOut = String.format("Try again the random number is %d and your guess is %d", randomNum, guess);
				System.out.println(strOut);
			}
		}

	}

}
