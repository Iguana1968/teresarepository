
public class FilmTest {

	public static void main(String[] args) {
		Film film = new Film();
		int actorsCount = 0;

		Actor actor1 = new Actor();
		actor1.name = "Al Pacino";
		film.addActor(actor1);
		
		Actor actor2 = new Actor();
		actor2.name = "Marlon Brando";
		film.addActor(actor2);
		
		Actor actor3 = new Actor();
		actor3.name = "Paul Newman";
		film.addActor(actor3);
		
		Actor actor4 = new Actor();
		actor4.name = "Daniel Craig";
		actorsCount = film.addActor(actor4);
		System.out.println("Total number of actors : " + actorsCount);
		
		
	}

}
