
public class Employee {
	private String name;
	private String jobTitle;
	private int employeeId;
	private int level;
	protected static int employeeIDCounter = 0;

	public void calculateEmployeeID() {
		this.employeeIDCounter++;
		setEmployeeId(employeeIDCounter);
		;

	}

	public void displayInformation() {
		String str = String.format("Employee : %s, Job title :  %s , Employee Id : %s, Level : %s", name, jobTitle,
				employeeId, level);
	}

	public String getName() {
		return name;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public int getLevel() {
		return level;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	private void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public void setLevel(int level) {
		this.level = level;
	}

}
