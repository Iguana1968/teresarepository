import java.util.ArrayList;

public class Initializers {

	public static void main(String[] args) throws Throwable {

		Customer c1 = new Customer("");
		
		c1.finalize();
		
		//System.out.println(c1.id); // 2
//		Customer c2 = new Customer();
//		System.out.println(c2.id); // 3
//		Customer c3 = new Customer();
//		System.out.println(c3.id); // 4
//		Customer c4 = new Customer();
//		System.out.println(c4.id); // 5

//		System.out.println(c1.instances); // 4
//		System.out.println(c2.instances); // 4
//		System.out.println(c3.instances); // 4
//		System.out.println(c4.instances); // 4
//
//		System.out.println(Customer.instances); // 4

		
		
	}

}

class Customer {

	public static final double pi;
	static {
		// System.out.println(pi);
		pi = 0;
		System.out.println(pi);
	}
	public static int instances = 0; // 0
	static {
		System.out.println("A");
//		System.out.println(instances); // 0
		instances = 1;
//		System.out.println(instances); // 1
	}

	static {
		System.out.println("B");
	}
	
	public final String company;
	
	{
		id = 0;
	}
	public String firstName;
	public int id;
	{
		System.out.println("C");
		firstName = "Andr�";
		id = 3;
//		company = "";
	}
	{
		System.out.println("D");
		id = 4;
	}

	public Customer() {
		System.out.println("E");
		id = ++instances;
		company = "";
	}
	
	public Customer(String firstName) {
		this();
		System.out.println("F");
	}

	
	@Override // Destructors
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		super.finalize();
		
	}
	
	
}

class Console {

	public static int instances;

	public static Console INSTANCE /* = new Console(1) */; // Eager //null
	public static int number = 1;
	public static int staticVar = 0;

	static {
		number = 2;
	}

	public ArrayList<String> names = new ArrayList<>(10);

	public int instanceVar = 0;

	{
		names = new ArrayList<>(20); // 16
		instanceVar++;
	}

	public Console(int instanceVar) {
		names = new ArrayList<>(30); // eager
		this.instanceVar = instanceVar;
		number = 3;
		instances++;
	}

	public static Console getInstance() {
		if (INSTANCE == null)
			INSTANCE = new Console(1); // Lazy

		return INSTANCE;
	}

}