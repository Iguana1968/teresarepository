import java.io.IOException;
import java.util.Scanner;

public class ExceptionsDemo {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		System.out.println("Hello");
		method1();
		System.out.println("Exit");
		
		Account a = new Account();
		a.methodHandle();
		
		try {
			a.methodDeclare();	
		} catch(IOException e) {
			
		}
		
		try {
			
		} finally {
			
		}
		
		a.methodDeclare();
		
	}

	public static void method1() {

		Scanner scanner = null;

		try {
			scanner = new Scanner(System.in);
			Account account = new Account();
			account.validateAccount("fcoelho", "12345");
			// String str = scanner.nextLine();

			System.out.println("C�digo");

		} catch (IllegalArgumentException e) {
			System.out.println("Tente de novo. Algo aconteceu");

		} catch (RuntimeException e) {

			System.out.println("Tente de novo. Algo aconteceu");

//		} catch (IOException e) {
//			System.out.println(e);
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			if (scanner != null)
				scanner.close();
			System.out.println("SEMPRE SER EXECUTADO");
		}
	}

}

class Account {

	public boolean validateAccount(String un, String pass) {
		if (un == null || pass == null)
			throw new IllegalArgumentException("");

		if (un.equals("fcoelho") && pass.equals("122345"))
			return true;

		// conex�o � BD
		// throw new SQLException();

		throw new RuntimeException("Username e password n�o est�o acessiveis"); // Throw point
	}
	
	public void methodHandle() {
		try {
			throw new IOException("");	
		} catch (IOException e) {
			
		}
	}
	
	public void methodDeclare() throws IOException {
		throw new IOException();
	}
}
