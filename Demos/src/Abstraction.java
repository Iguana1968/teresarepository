
public class Abstraction {
	public static void main(String[] args) {

		Driver driver = new Driver();
		driver.car = new Car();

		driver.startEngine();

		driver.accelerate();

		boolean started = driver.car.engine.started;

	}

}

class Driver {

	public Car car;

	public void startEngine() {
		car.startEngine();

	}

	public void accelerate() {
		car.accelerate();
	}

	public void lockDoors() {
		car.lockDoors();
	}

}

class Car {

	public Engine engine; // null

	public Car() {
		engine = new Engine();
	}

	public void accelerate() {
		if (!teste()) {
			System.out.println("Engine needs to be started");
		} else {
			engine.accelerate();
		}
	}

	private boolean teste() {
		return engine.started;
	}

	public void startEngine() {
		engine.started = !engine.started;
	}

	public void lockDoors() {

	}
}

class Engine {

	public boolean started;

	public void accelerate() {

	}
}

class Gasoline {

}