
public class Employee {

	private String firstName;
	private String lastName;
	private double monthlySalary;

	public Employee(String fName, String lName, double mSalary) {

		firstName = fName;
		lastName = lName;
		monthlySalary = mSalary;

	}
	

	private String getFirstName() {
		return firstName;
	}

	private String getLastName() {
		return lastName;
	}

	private double getMonthlySalary() {
		return monthlySalary;
	}

	private void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	private void setLastName(String lastName) {
		this.lastName = lastName;
	}

	private void setMonthlySalary(double monthlySalary) {
		this.monthlySalary = monthlySalary;
	}

	
}
