
public class Square {

	private int width;
	private int height;

	public Square() {
		width = 5;
		height = 5;
		System.out.println("Default rectangle created: width = 5, height = 5");

	}

	public Square(int w, int h) {
		if (w > 0 && w < 30 && h == w) {
			width = w;
			height = h;
		} else {
			System.out.println("Wrong dimensions");
		}
	}

	public int getArea(int width, int height) {
		return width * height;
	}

	public void draw() {
		for (int row = 0; row < height; row++) {
			for (int col = 0; col < width; col++) {
				System.out.print("*");
			}
			System.out.println();
		}
	}
}
