
public class CategoryTest {

	public static void main(String[] args) {
		Category cat1 = new Category();
		Category cat2 = new Category();
		cat1.setCategoryInfo(1, "Category 1 name");
		cat2.setCategoryInfo(2, "Category 2 name", 2);

		cat1.displayCategory();
		cat2.displayCategory();
	}

}
