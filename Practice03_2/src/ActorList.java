import java.util.ArrayList;

public class ActorList {
	public ArrayList<String> listOfActors;

	public void setList() {
		listOfActors = new ArrayList<String>();
		listOfActors.add("Joe Smith");
		listOfActors.add("Teresa Pinho");
		listOfActors.add("Jose Pinho");
		listOfActors.add("Helder Pimenta");

		System.out.println(listOfActors);

		System.out.println("List of actors size : " + listOfActors.size());

	}

	public void manipulateList() {

		listOfActors.remove("Joe Smith");

		System.out.println(" List of Actors after removing \"Joe Smith\" :" + listOfActors);
		System.out.println(" Listof Actors size : " + listOfActors.size());

		listOfActors.add(0, "Joe Smith");
		System.out.println(" List of Actors after inserting \"Joe Smith\" :" + listOfActors);
		System.out.println(" Listof Actors size : " + listOfActors.size());

		for (var item : listOfActors) {
			System.out.println(item.strip());
		}

	}
}
