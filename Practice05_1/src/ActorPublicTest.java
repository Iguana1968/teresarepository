
public class ActorPublicTest {

	public static void main(String[] args) {
		ActorPublic actorPublic = new ActorPublic();
		actorPublic.actorId = 0;
		actorPublic.firstName = "Sean";
		actorPublic.lastName = "Connery";
		System.out.println("Actor Id :" + actorPublic.actorId + " " + "Actor Name : " + actorPublic.firstName + " "
				+ actorPublic.lastName);

		ActorPublic actorPublic1 = new ActorPublic();
		actorPublic1.actorId = 1;
		actorPublic1.firstName = "Daniel";
		actorPublic1.lastName = null;
		System.out.println("Actor Id :" + actorPublic1.actorId + " " + "Actor Name : " + actorPublic1.firstName + " "
				+ actorPublic1.lastName);

		ActorPrivate actorPrivate = new ActorPrivate();
		actorPrivate.setActorId(2);
		actorPrivate.setFirstName("Marlon");
		actorPrivate.setLastName(null);
		System.out.println("Actor Id :" + actorPrivate.getActorId() + " " + "Actor Name : " + actorPrivate.getFirstName() + " "
				+ actorPrivate.getLastName());

	}

}
