
public class ActorPrivate {
	private int actorId;
	private String firstName;
	private String lastName;

	public int getActorId() {
		return actorId;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setActorId(int actorId) {
		this.actorId = actorId;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		if (lastName == null)
			this.lastName = "No name assigned";
		else
			this.lastName = lastName;
	}

}
