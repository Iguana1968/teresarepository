import java.util.Scanner;

public class FilmRentalArray {

	int[] filmsRentedDiscount;
	int filmsRented;

	public void setFilmRentalArray() {
		filmsRentedDiscount = new int[7];

		filmsRentedDiscount[0] = 0;
		filmsRentedDiscount[1] = 15;
		filmsRentedDiscount[2] = 15;
		filmsRentedDiscount[3] = 15;
		filmsRentedDiscount[4] = 20;
		filmsRentedDiscount[5] = 20;
		filmsRentedDiscount[6] = 25;

	}

	public void displayFilmsRentedDiscount() {
		int discount = 0;
		int originalNumberOfFilmes = filmsRented;
		if (filmsRented > 6)
			filmsRented = 6;
		discount = filmsRentedDiscount[filmsRented];
		String strOutput = String.format("Your discount is %d for %d films rented", discount, originalNumberOfFilmes);
		System.out.println(strOutput);
	}

}
