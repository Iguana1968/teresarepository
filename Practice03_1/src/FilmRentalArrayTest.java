import java.util.Scanner;

public class FilmRentalArrayTest {

	public static void main(String[] args) {
		FilmRentalArray filmRentalArray = new FilmRentalArray();
		filmRentalArray.setFilmRentalArray();

		Scanner scanner = new Scanner(System.in);

		int numberOfFilms = 0;

		do {
			System.out.println("Please enter the number of films to rent :");
			numberOfFilms = scanner.nextInt();

			if (numberOfFilms <= 0)
				System.out.println("Invalid number of films requested");
			
		} while (numberOfFilms <= 0);

		
		filmRentalArray.filmsRented = numberOfFilms;

		filmRentalArray.displayFilmsRentedDiscount();

	}

}
