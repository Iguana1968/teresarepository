
public class ColorTest {

	public static void main(String[] args) {
		
		System.out.println("\u001b[31mThis is a red color");
		System.out.println("\u001b[31mHello");
		System.out.println("\u001b[38;5;1mHello");

		for(int i = 0; i < 16; i++) {
			for(int j = 0; j < 16; j++) {
					String code = (i * 16 + j + "");
					System.out.printf("%s%s%s %4s", "\u001b[38;5;", code,  "m", code );
				}
				System.out.println();
			}

	}

}
